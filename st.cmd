require essioc
require adsis8300bcm

# set macros
epicsEnvSet("CONTROL_GROUP", "PBI-BCM05")
epicsEnvSet("AMC_NAME",      "Ctrl-AMC-110")
epicsEnvSet("AMC_DEVICE",    "/dev/sis8300-5")
epicsEnvSet("EVR_NAME",      "PBI-BCM05:Ctrl-EVR-101:")

epicsEnvSet("P",        "$(CONTROL_GROUP):")
epicsEnvSet("R",        "$(AMC_NAME):")
epicsEnvSet("PREFIX",   "$(P)$(R)")

# set BCM generic channel names
iocshLoad("bcm-channels.iocsh")

# set BCM specific channel names
epicsEnvSet("SYSTEM1_PREFIX",  "HEBT-010LWU:PBI-BCM-001:")
epicsEnvSet("SYSTEM1_NAME",    "PBI-BCM05#1 - HEBT-010")
epicsEnvSet("SYSTEM1_ARCHIVER","")










# load BCM data acquistion
iocshLoad("$(adsis8300bcm_DIR)/bcm-main.iocsh", "CONTROL_GROUP=$(CONTROL_GROUP), AMC_NAME=$(AMC_NAME), AMC_DEVICE=$(AMC_DEVICE), EVR_NAME=$(EVR_NAME)")

# load common module
iocshLoad $(essioc_DIR)/common_config.iocsh

# custom autosave in order to have Lut ID PVs restored before the others
afterInit("makeAutosaveFileFromDbInfo('$(AS_TOP)/$(IOCDIR)/req/lutIDs.req','autosaveFieldsLutIDs')")
afterInit("create_monitor_set("lutIDs.req",5)")
afterInit("fdbrestore("$(AS_TOP)/$(IOCDIR)/save/lutIDs.sav")")
afterInit("epicsThreadSleep(2)")

afterInit("fdbrestore("$(AS_TOP)/$(IOCDIR)/save/settings.sav")")

# call iocInit
iocInit

date

